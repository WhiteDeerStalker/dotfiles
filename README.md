# dotfiles

Collection of dotfiles organized by user named folders. The dot files included are largely copied from https://github.com/geerlingguy/dotfiles.

## License

MIT / BSD

